package com.zv.freelansimclient.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import com.zv.freelansimclient.*;
import com.zv.freelansimclient.models.Task;
import com.zv.freelansimclient.models.ShortTask;

/**
 * Created with IntelliJ IDEA.
 * User: dimon
 * Date: 17.06.13
 * Time: 12:52
 * Подробная информация о заказе
 */
public class TaskActivity extends Activity {
    public static final String EXT_ITEM_TASK = "item_task";
    public static final String TASK_KEY = "task";

    private TextView viewsTextView;
    private TextView commentsTextView;
    private TextView publishedTextView;
    private TextView priceTextView;
    private TextView tagsTextView;
    private TextView infoTextView;
    private TextView paymentTextView;

    private Task mTask;

    private Bundle mExtras;

    //некоторая информация о фрилансере передается через extras
    private ShortTask mItem;
    private GetTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_task);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        viewsTextView = (TextView)findViewById(R.id.viewsTextView);
        commentsTextView = (TextView)findViewById(R.id.commentsTextView);
        publishedTextView = (TextView)findViewById(R.id.publishedTextView);
        priceTextView = (TextView)findViewById(R.id.priceTextView);
        tagsTextView = (TextView)findViewById(R.id.tagsTextView);
        infoTextView = (TextView)findViewById(R.id.infoTextView);
        paymentTextView = (TextView)findViewById(R.id.paymentTextView);

        mExtras = getIntent().getExtras();
        mItem = mExtras.getParcelable(EXT_ITEM_TASK);

        tagsTextView.setText(mItem.getTags());
        priceTextView.setText(mItem.getPrice());

        setTitle(mItem.getTitle());


        if(mTask == null){
            if(Utils.isOnline(this)) {
                task = new GetTask();
                task.execute(mItem.getId());
            } else {
                Toast.makeText(this, R.string.check_internet_status, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(task != null) {
            task.cancel(true);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(TASK_KEY, mTask);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mTask = savedInstanceState.getParcelable(TASK_KEY);
        fill(mTask);
    }

    private void fill(Task task){
        infoTextView.setText(Html.fromHtml(task.getInfo()));
        viewsTextView.setText(String.valueOf(task.getViews()));
        commentsTextView.setText(String.valueOf(task.getComments()));
        publishedTextView.setText(task.getPublished());
        paymentTextView.setText(task.getPayment());
    }

    private class GetTask extends AsyncTask<Integer, Void, Task> {

        @Override
        protected Task doInBackground(Integer... params) {
            return new FreelansimJsoupParser(TaskActivity.this).getTask(params[0]);
        }

        @Override
        protected void onPostExecute(Task task) {
            super.onPostExecute(task);

            mTask = task;

            fill(task);
        }
    }
}
