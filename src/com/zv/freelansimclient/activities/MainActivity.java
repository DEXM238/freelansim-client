/**
 * Является контейнером для списка заказов и фрилансеров. Содержит боковое меню
 */

package com.zv.freelansimclient.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.zv.freelansimclient.adapters.CategoriesAdapter;
import com.zv.freelansimclient.DrawerTextView;
import com.zv.freelansimclient.fragments.FreelancersListFragment;
import com.zv.freelansimclient.R;
import com.zv.freelansimclient.fragments.TasksListFragment;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshAttacher;

import java.security.InvalidParameterException;


public class MainActivity extends FragmentActivity{

    /**
     * позиция фрагмента FreelancersListFragment в списке
     */
    private static final int FREELANCERS_LIST_POSITION = 1;
    /**
     * позиция фрагмента TasksListFragment в списке
     */
    private static final int TASKS_LIST_POSITION = 0;

    private static final String LAST_FRAGMENT = "last_fragment";
    private static final String LAST_FRAGMENT_TITLE_POSITION = "title_position";

    private SharedPreferences mPreferences;

    //UI START
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerLayoutLeft;
    /**
     * содержит навигационные элементы
     */
    private View mDrawerHeader;
    private DrawerTextView mTasksDrawer;
    private DrawerTextView mFreelansersDrawer;
    private LinearLayout mAuthDrawerItem;
    //UI END

    private ActionBarDrawerToggle mDrawerToggle;
    private PullToRefreshAttacher mPullToRefreshAttacher;
    private DrawerItemClickListener mDrawerItemClickListener;

    private Fragment mCurrentFragment;
    private FragmentManager mFragmentManager;

    /**
     * Заголовок Activity
     */
    private String[] mDrawerTitles;
    private String[] mCategoriesTitles;
    private String[] mCategoriesTitlesValues;
    private Resources mResources;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mPreferences = getPreferences(MODE_PRIVATE);
        mResources = getResources();
        mPullToRefreshAttacher = new PullToRefreshAttacher(this);
        mDrawerItemClickListener = new DrawerItemClickListener();

        mDrawerTitles = mResources.getStringArray(R.array.drawer_menu);
        mCategoriesTitles = mResources.getStringArray(R.array.categories);
        mCategoriesTitlesValues = mResources.getStringArray(R.array.categories_values);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayoutLeft = (ListView)findViewById(R.id.categoriesListView);
        mDrawerHeader = getLayoutInflater().inflate(R.layout.drawer_header, null);
        mTasksDrawer = (DrawerTextView)mDrawerHeader.findViewById(R.id.tasksDrawerTextView);
        mFreelansersDrawer = (DrawerTextView)mDrawerHeader.findViewById(R.id.freelansersDrawerTextView);
        mAuthDrawerItem = (LinearLayout)mDrawerHeader.findViewById(R.id.authDrawerItem);

        mTasksDrawer.setOnClickListener(mDrawerItemClickListener);
        mTasksDrawer.setDrawerItemId(TASKS_LIST_POSITION);

        mFreelansersDrawer.setOnClickListener(mDrawerItemClickListener);
        mFreelansersDrawer.setDrawerItemId(FREELANCERS_LIST_POSITION);

        mDrawerLayoutLeft.addHeaderView(mDrawerHeader, null, false);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerLayoutLeft.setAdapter(new CategoriesAdapter(this, mCategoriesTitles, mCategoriesTitlesValues));

        mAuthDrawerItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AuthActivity.class));
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.empty,
                R.string.order_description
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);


        //Установка фрагмента, который отображался перед выоходом из приложения
        String lastFragment = mPreferences.getString(LAST_FRAGMENT, TasksListFragment.class.getName());
        int position = mPreferences.getInt(LAST_FRAGMENT_TITLE_POSITION, TASKS_LIST_POSITION);
        if(position == TASKS_LIST_POSITION){
            mTasksDrawer.setHighlight(true);
        } else if(position == FREELANCERS_LIST_POSITION){
            mFreelansersDrawer.setHighlight(true);
        }
        setTitle(mDrawerTitles[position]);

        try {
            mCurrentFragment = (Fragment) Class.forName(lastFragment).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.beginTransaction().replace(R.id.content_frame, mCurrentFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerLayoutLeft);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch(item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    /**
     * Заменяет фрагмент в Frame Layout
     * @param position позиция в списке, которая указывает на фрагмент
     */
    private void selectItem(int position) {
        SharedPreferences.Editor editor = mPreferences.edit();
        //выбираем нужный список в зависимости от выбранной позиции
        if(position == FREELANCERS_LIST_POSITION) {
            mCurrentFragment = new FreelancersListFragment();

            mTasksDrawer.setHighlight(false);
            mFreelansersDrawer.setHighlight(true);

            editor.putString(LAST_FRAGMENT, FreelancersListFragment.class.getName());
            editor.putInt(LAST_FRAGMENT_TITLE_POSITION, FREELANCERS_LIST_POSITION);
        } else if(position == TASKS_LIST_POSITION){
            mCurrentFragment = new TasksListFragment();

            mTasksDrawer.setHighlight(true);
            mFreelansersDrawer.setHighlight(false);

            editor.putString(LAST_FRAGMENT, TasksListFragment.class.getName());
            editor.putInt(LAST_FRAGMENT_TITLE_POSITION, TASKS_LIST_POSITION);
        }

         //TODO:   throw new InvalidParameterException("Unknown drawer item id");

        editor.commit();

        mFragmentManager.beginTransaction().replace(R.id.content_frame, mCurrentFragment).commit();
        setTitle(mDrawerTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerLayoutLeft);
    }

    public PullToRefreshAttacher getPullToRefreshAttacher() {
        return mPullToRefreshAttacher;
    }

    private class DrawerItemClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            DrawerTextView item;
            try {
            item = (DrawerTextView) v;
            } catch(ClassCastException e){
                throw new InvalidParameterException("This listener must be used for DrawerTextView");
            }

            selectItem(item.getDrawerItemId());
        }
    }
}