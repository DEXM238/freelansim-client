package com.zv.freelansimclient.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import com.zv.freelansimclient.DrawableManager;
import com.zv.freelansimclient.FreelansimJsoupParser;
import com.zv.freelansimclient.R;
import com.zv.freelansimclient.models.Freelancer;
import com.zv.freelansimclient.models.ShortFreelancer;

/**
 * Created with IntelliJ IDEA.
 * User: Zlyuka
 * Date: 20.07.13
 * Time: 0:02
 * To change this template use File | Settings | File Templates.
 */
public class FreelancerActivity extends Activity {
    private static final int MENU_CALL = 1;
    private static final int MENU_MAIL = 2;
    private static final int MENU_SITE = 3;

    private static final String FREELANCER_KEY= "freelancer";

    private Freelancer mFreelancer;


    public static final String EXT_ITEM_FREELANCER = "freelancer";

    private TextView priceTextView;
    private TextView aboutTextView;
    private TextView tagsTextView;
    private TextView ageTextView;
    private TextView experienceTextView;
    private TextView ownershipTextView;
    private TextView specialtyTextView;
    private ImageView avatarImageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);;

        setContentView(R.layout.activity_detail_freelancer);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        priceTextView = (TextView)findViewById(R.id.priceTextView);
        aboutTextView = (TextView)findViewById(R.id.aboutTextView);
        tagsTextView = (TextView)findViewById(R.id.tagsTextView);
        ageTextView = (TextView)findViewById(R.id.ageTextView);
        experienceTextView = (TextView)findViewById(R.id.experiencTextView);
        ownershipTextView = (TextView)findViewById(R.id.ownershipTextView);
        specialtyTextView = (TextView)findViewById(R.id.specialtyTextView);
        avatarImageView = (ImageView)findViewById(R.id.avatarImageView);

        Bundle extras = getIntent().getExtras();
        ShortFreelancer item = extras.getParcelable(EXT_ITEM_FREELANCER);

        priceTextView.setText(item.getPrice());
        aboutTextView.setText(item.getText());
        tagsTextView.setText(item.getTags());

        String avatar = item.getAvatar();
        if(avatar != null){
            new DrawableManager().fetchDrawableOnThread("http://freelansim.ru" + item.getAvatar(), avatarImageView);
        }

        setTitle(item.getName());

        if(mFreelancer == null){
            new GetDetailFrelancer().execute(item.getUsername());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_freelancer, menu);

        if(mFreelancer == null)
            return true;

        if(mFreelancer.getPhone() != null){
            menu.add(0, MENU_CALL, 0,  R.string.menu_call)
                    .setIcon(R.drawable.menu_call)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }

        if(mFreelancer.getMail() != null){
            menu.add(0, MENU_MAIL, 0,  R.string.menu_new_mail)
                    .setIcon(R.drawable.menu_new_mail)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }

        if(mFreelancer.getSite() != null){
            menu.add(0, MENU_SITE, 0,  R.string.menu_site)
                    .setIcon(R.drawable.menu_web_site)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case MENU_CALL:
                startAction(Intent.ACTION_DIAL, Uri.parse("tel:" + mFreelancer.getPhone()));
                return true;
            case MENU_MAIL:
                startAction(Intent.ACTION_SEND, Uri.parse("mailto:" + mFreelancer.getMail()));
                return true;
            case MENU_SITE:
                startAction(Intent.ACTION_VIEW, Uri.parse(mFreelancer.getSite()));
                return true;
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(FREELANCER_KEY, mFreelancer);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        Freelancer freelancer = savedInstanceState.getParcelable(FREELANCER_KEY);

        mFreelancer = freelancer;
        invalidateOptionsMenu();
        fill(freelancer);
    }

    /**
     * Заполняет поля в интерфейсе
     * @param freelancer
     */
    private void fill(Freelancer freelancer){
        if(freelancer != null){
            aboutTextView.setText(Html.fromHtml(freelancer.getAbout()));
            ageTextView.setText(freelancer.getAge());
            experienceTextView.setText(freelancer.getExperience());
            ownershipTextView.setText(freelancer.getOwnership());
            specialtyTextView.setText(freelancer.getSpecialty());
        }
    }

    /**
     * Запускает Activity
     */
    private void startAction(String action, Uri uri){
        Intent i = new Intent(action, uri);
        startActivity(i);
    }

    private class GetDetailFrelancer extends AsyncTask<String, Void, Freelancer> {

        @Override
        protected Freelancer doInBackground(String... params) {
            return new FreelansimJsoupParser(FreelancerActivity.this).getFreelancer(params[0]);
        }

        @Override
        protected void onPostExecute(Freelancer freelancer) {
            super.onPostExecute(freelancer);
            mFreelancer = freelancer;

            //если номер не пустой, то добавляем кнопку в actionn bar
            invalidateOptionsMenu();

            if(mFreelancer != null){
                fill(freelancer);
            }
        }
    }
}