package com.zv.freelansimclient;

import android.content.Context;
import android.content.res.Resources;
import com.zv.freelansimclient.adapters.CategoriesAdapter;
import com.zv.freelansimclient.models.Freelancer;
import com.zv.freelansimclient.models.ShortFreelancer;
import com.zv.freelansimclient.models.ShortTask;
import com.zv.freelansimclient.models.Task;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dimon
 * Date: 08.06.13
 * Time: 12:10
 * To change this template use File | Settings | File Templates.
 */
public class FreelansimJsoupParser implements Freelansim{
    private static final String SITE_URL = "http://freelansim.ru/";
    private static final String TASKS_TYPE = "tasks";
    private static final String TASKS_DETAIL_URL = "http://freelansim.ru/tasks/";
    private static final String FREELANCERS_TYPE = "freelancers";
    private static final String FRELANCER_DETAIL_URL = "http://freelansim.ru/freelancers/";
    private static final String DEFAULT_SMALL_AVATAR = "/assets/default/photos/users/default_r100.png";

    private Context mContext;
    private Resources mResources;
    private Document mDoc = null;

    public FreelansimJsoupParser(Context mContext) {
        this.mContext = mContext;
    }

    public List<ShortTask> getShortTasksList(int page) {
        if(!connect(buildQuery(TASKS_TYPE, page, getCategories(), "")))
            return null;

        Elements shortcutsItem = mDoc.select("div.shortcuts_item");

        List<ShortTask> result = new ArrayList<ShortTask>(shortcutsItem.size());
        ShortTask item = null;

        for(Element element: shortcutsItem) {
            try {
                item = new ShortTask(
                        //заголовок
                        element.child(0).attr("title"),
                        //цена
                        element.child(1).text(),
                        //время публикации
                        element.child(2).text(),
                        //теги
                        tagsSeparatedComa(element.child(4).children()),
                        //id
                        Integer.parseInt(element.child(0).child(0).attr("href").substring(7))
                );
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }
            result.add(item);
        }
        return result;
    }

    public Task getTask(int taskId) {
        if(!connect(TASKS_DETAIL_URL + taskId))
            return null;

        Element views = mDoc.select("div.views").first();            //просмотры
        Element comments = views.nextElementSibling();              //отклики

        try {
            return new Task(
                mDoc.select("div.title").first().text(),     //заголовок
                mDoc.select("div.text").first().html(),      //информация
                Integer.parseInt(views.text()),         //просмотры
                Integer.parseInt(comments.text()),      //отклики
                comments.nextElementSibling().text(),       //дата публикации
                mDoc.select("tr").first().child(2).text(),   //форма оплаты
                tagsSeparatedComa(mDoc.select("div.tags").first().children()),//теги
                mDoc.select("div.price").first().text()      //цена
            );
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<ShortFreelancer> getShortFreelancersList(int page, String query) {
        if(!connect(buildQuery(FREELANCERS_TYPE, page, getCategories(), query)))
            return null;

        List<ShortFreelancer> result = new ArrayList<ShortFreelancer>();
        Elements freelancers = mDoc.select("div.freelancer");
        Element info;
        Element name;
        ShortFreelancer shortFreelancerItem;
        String avatar;

        for(Element freelancer: freelancers) {
            info = freelancer.child(1);
            name = info.child(2);
            String username = name.child(0).attr("href");

            //если  стандартный аватар, то не записываем
            avatar = freelancer.child(0).child(0).child(0).attr("src");
            if(avatar.equals(DEFAULT_SMALL_AVATAR))
                avatar = null;

            shortFreelancerItem = new ShortFreelancer(
                //имя
                name.text(),
                //ссылка на аватар
                avatar,
                //описание
                info.child(3).text(),
                //текст
                info.child(4).text(),
                //теги
                tagsSeparatedComa(info.child(5).children()),
                //статус
                info.child(1).text(),
                //цена
                info.child(0).text(),
                username.substring(13, username.length())
            );

            result.add(shortFreelancerItem);
        }
        return result;
    }

    public Freelancer getFreelancer(String freelancerName) {
        if(!connect(FRELANCER_DETAIL_URL + freelancerName))
            return null;

        //скрытый профиль
        if(mDoc.select(".hidden_profile").size() > 0){
            return null;
        }

        Element moreInformation = mDoc.select("div.more_information").first();
        Element avatar = mDoc.select(".avatar").first();
        Elements allTags = mDoc.select(".tags");
        Element about = mDoc.select(".self_about").first();
        Element short_info = mDoc.select(".short_info").first();
        Element specialty = mDoc.select(".specialty").first();
        Element name = short_info.child(2).child(0);
        Element infoTable =  moreInformation.select("table").first().child(0);
        Elements contacts = mDoc.select(".contacts");


        String site = null;
        String mail = null;
        String phone = null;

        if(contacts.size() > 0) {
            Element contact = contacts.first();

            Elements phoneElements = contact.select(".phone");
            Elements mailElements = contact.select(".mail");
            Elements siteElements = contact.select(".site");

            if(phoneElements.size() > 0) {
                phone = phoneElements.first().attr("data-phone");
            }

            if(mailElements.size() > 0) {
                mail = mailElements.first().attr("data-mail-name") + '@' + mailElements.first().attr("data-mail-host");
            }

            if(siteElements.size() > 0) {
                site = siteElements.first().attr("href");
            }
        }

        String age = null;
        String experience = null;
        String ownership = null;

        Elements info = infoTable.children();
        if(info.size() > 0) {
            for (Element currentInfo : info) {
                if (currentInfo.child(0).text().contentEquals("Возраст:"))
                    age = currentInfo.child(1).text();
                else if (currentInfo.child(0).text().contentEquals("Опыт работы:"))
                    experience = currentInfo.child(1).text();
                else if (currentInfo.child(0).text().contentEquals("Форма собственности:"))
                    ownership = currentInfo.child(1).text();
            }
        }

        String mentalTags = null;
        if(allTags.size() > 1) {
            mentalTags = tagsSeparatedComa(allTags.get(1).children());
        }

        String location = null;
        Elements locationElements = short_info.select(".location");
        if(locationElements.size() > 0) {
            location = locationElements.first().text();
        }


        return new Freelancer(
            short_info.child(2).child(0).text(),//имя
            avatar.child(0).attr("src"), //аватар
            about.html(), // описание фрилансера
            tagsSeparatedComa(allTags.get(0).children()), //теги обязательно
            name.child(0).text(),//статус
            short_info.child(0).text(), //цена
            location, //город
            age, //возраст
            experience, //опыт
            ownership,//форма собственности
            mentalTags, //личностные качества
            specialty.text(),//специализация
            phone,
            mail,
            site
        );
    }

    private String tagsSeparatedComa(Elements tags) {
        StringBuilder sb = new StringBuilder();
        for(Element e : tags) {
            sb.append(e.text());
            sb.append(", ");
        }
        sb.delete(sb.length()-2, sb.length());
        return sb.toString();
    }

    private boolean connect(String url) {
        if(!Utils.isOnline(this.mContext)) {
            return false;
        }

        try {
            mDoc = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return false;
        }
        return true;
    }


    /**
     * Фильтрация запроса по категориям
     * @return массив, с названиями отмеченных категорий
     */
    private String[] getCategories(){

        if(mResources == null){
            mResources = mContext.getResources();
        }

        List<String> result = new ArrayList<String>();

        String[] subCategories;
        boolean[] subCategoriesChecked;

        //названия категорий: web, mobile, app и т.д.
        String[] categories = mContext.getResources().getStringArray(R.array.categories_values);

        //отмеченные категории
        boolean[] сategoriesChecked = Utils.readBooleanArrayPref(mContext, CategoriesAdapter.CATEGORIES_PREFS);


        if(сategoriesChecked != null){
            for(int i=0; i < сategoriesChecked.length; i++){

                if(сategoriesChecked[i]){

                    //массив со значениями подкатегорий: ios, android и т.д.
                    int subCategoriesResId = mResources.getIdentifier(categories[i]+"_values", "array", mContext.getPackageName());

                    int subCategoriesCheckedResId = mResources.getIdentifier(categories[i], "array", mContext.getPackageName());

                    subCategories = mResources.getStringArray(subCategoriesResId);

                    subCategoriesChecked = Utils.readBooleanArrayPref(mContext,
                            mResources.getResourceName(subCategoriesCheckedResId));

                    if(subCategoriesChecked != null){
                        for(int j = 0; j < subCategoriesChecked.length; j++){
                            if(subCategoriesChecked[j])
                                result.add(subCategories[j]);
                        }
                    }
                }
            }
        }

        return result.toArray(new String[result.size()]);
    }

    /**
     * Строит корректный URL для запроса к серверу
     * @param itemType тип элементов (задачи, фрилансеры)
     * @param page номер страницы
     * @param categories список запрашиваемых категорий (android, web_all_inclusive и т.д.)
     * @return корректный URL
     */
    private String buildQuery(String itemType, int page, String[] categories, String query){
        String result = SITE_URL;

        result+= itemType;

        result += "?page=" + page;

        result += "&categories=";

        for (String category : categories) {
            result += category + ',';
        }

        result += "&q=" + query;

        return result;
    }
}
