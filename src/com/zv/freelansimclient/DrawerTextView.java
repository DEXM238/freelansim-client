package com.zv.freelansimclient;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: Zlyuka
 * Date: 30.08.13
 * Time: 23:53
 * To change this template use File | Settings | File Templates.
 */
public class DrawerTextView extends TextView {
    private Drawable highlightDrawable;

    /**
     * идентификатор действия, за которое отвечает пункт
     */
    private int mDrawerItemId;

    public DrawerTextView(Context context) {
        super(context);
        highlightDrawable = getResources().getDrawable(R.drawable.list_selector_pressed);
    }

    public DrawerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        highlightDrawable = getResources().getDrawable(R.drawable.list_selector_pressed);
    }

    public DrawerTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        highlightDrawable = getResources().getDrawable(R.drawable.list_selector_pressed);
    }

    public void setHighlight(boolean checked){
        int paddingBottom = this.getPaddingBottom();
        int paddingTop = this.getPaddingTop();
        int paddinfLeft = this.getPaddingLeft();
        int paddinfRight = this.getPaddingRight();

        if(checked){
            this.setBackgroundDrawable(highlightDrawable);
            this.setPadding(paddinfLeft, paddingTop, paddinfRight, paddingBottom);
        } else {
            this.setBackgroundColor(Color.TRANSPARENT);
            this.setPadding(paddinfLeft, paddingTop, paddinfRight, paddingBottom);

        }
    }

    public int getDrawerItemId() {
        return mDrawerItemId;
    }

    public void setDrawerItemId(int mDrawerItemId) {
        this.mDrawerItemId = mDrawerItemId;
    }
}
