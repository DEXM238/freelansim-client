package com.zv.freelansimclient.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created with IntelliJ IDEA.
 * User: dimon
 * Date: 30.06.13
 * Time: 13:03
 * To change this template use File | Settings | File Templates.
 */
public class ShortFreelancer implements Parcelable{
    private String name;
    private String avatar;
    private String description;
    private String text;
    private String tags;
    private String status;
    private String price;
    private String username;

    public ShortFreelancer(String name, String avatar, String description,
                           String text, String tags, String status, String price, String username) {
        this.name = name;
        this.avatar = avatar;
        this.description = description;
        this.text = text;
        this.tags = tags;
        this.status = status;
        this.price = price;
        this.username = username;
    }

    public ShortFreelancer(Parcel in) {
        String[] data = new String[8];

        in.readStringArray(data);
        this.name = data[0];
        this.avatar = data[1];
        this.description = data[2];
        this.text = data[3];
        this.tags = data[4];
        this.status = data[5];
        this.price = data[6];
        this.username = data[7];
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getDescription() {
        return description;
    }

    public String getText() {
        return text;
    }

    public String getTags() {
        return tags;
    }

    public String getStatus() {
        return status;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {
                this.name,
                this.avatar,
                this.description,
                this.text,
                this.tags,
                this.status,
                this.price ,
                this.username
        });
    }

    public static final Creator<ShortFreelancer> CREATOR = new Creator<ShortFreelancer>() {
        @Override
        public ShortFreelancer createFromParcel(Parcel in) {
            return new ShortFreelancer(in);
        }

        @Override
        public ShortFreelancer[] newArray(int size) {
            return new ShortFreelancer[size];
        }
    };
}
