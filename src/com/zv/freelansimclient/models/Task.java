package com.zv.freelansimclient.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created with IntelliJ IDEA.
 * User: Zlyuka
 * Date: 11.06.13
 * Time: 20:07
 * To change this template use File | Settings | File Templates.
 */
public class Task implements Parcelable{
    private String title;
    private String info;
    private int views;
    private int comments;
    private String published;
    private String payment;
    private String price;
    private String tags;

    public Task(Parcel in){
        String[] data = new String[6];
        in.readStringArray(data);

        this.title = data[0];
        this.info = data[1];
        this.published = data[2];
        this.payment = data[3];
        this.price = data[4];
        this.tags = data[5];


        this.views = in.readInt();
        this.comments = in.readInt();
    }

    public Task(String title, String info, int views, int comments,
                String published, String payment, String tags, String price) {
        this.title = title;
        this.info = info;
        this.views = views;
        this.comments = comments;
        this.published = published;
        this.payment = payment;
        this.tags = tags;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public String getInfo() {
        return info;
    }

    public int getViews() {
        return views;
    }

    public String getPublished() {
        return published;
    }

    public int getComments() {
        return comments;
    }

    public String getPayment() {
        return payment;
    }

    public String getTags() {
        return tags;
    }
    public String getPrice() {
        return price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{
                this.title,
                this.info,
                this.published,
                this.payment,
                this.price,
                this.tags
        });
        dest.writeInt(this.views);
        dest.writeInt(this.comments);
    }

    private static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel source) {
            return new Task(source);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };
}
