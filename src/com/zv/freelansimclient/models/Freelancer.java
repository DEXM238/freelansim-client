package com.zv.freelansimclient.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created with IntelliJ IDEA.
 * User: Zlyuka
 * Date: 20.07.13
 * Time: 20:52
 * To change this template use File | Settings | File Templates.
 */
public class Freelancer implements Parcelable{
    private String name;
    private String avatar;
    private String about;
    private String tags;
    private String status;
    private String price;
    private String location;
    private String age;
    private String experience;
    private String ownership;
    private String mentalTags;
    private String specialty;
    private String phone;
    private String mail;
    private String site;

    public String getName() {
        return name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getAbout() {
        return about;
    }

    public String getTags() {
        return tags;
    }

    public String getStatus() {
        return status;
    }

    public String getSpecialty() {
        return specialty;
    }

    public String getMentalTags() {
        return mentalTags;
    }

    public String getExperience() {
        return experience;
    }

    public String getLocation() {
        return location;
    }

    public String getPrice() {
        return price;
    }

    public String getAge() {
        return age;
    }

    public String getOwnership() {
        return ownership;
    }

    public String getPhone() {
        return phone;
    }

    public String getMail() {
        return mail;
    }

    public String getSite() {
        return site;
    }

    public Freelancer(Parcel in){
        String[] data = new String[15];
        in.readStringArray(data);

        this.name = data[0];
        this.avatar = data[1];
        this.about = data[2];
        this.tags = data[3];
        this.status = data[4];
        this.price = data[5];
        this.location = data[6];
        this.age = data[7];
        this.experience = data[8];
        this.ownership = data[9];
        this.mentalTags = data[10];
        this.specialty = data[11];
        this.phone = data[12];
        this.mail = data[13];
        this.site = data[14];
    }


    public Freelancer(String name, String avatar, String about,
                      String tags, String status, String price, String location,
                      String age, String experience, String ownership,
                      String mentalTags, String specialty, String phone, String mail, String site) {
        this.name = name;
        this.avatar = avatar;
        this.about = about;
        this.tags = tags;
        this.status = status;
        this.price = price;
        this.location = location;
        this.age = age;
        this.experience = experience;
        this.ownership = ownership;
        this.mentalTags = mentalTags;
        this.specialty = specialty;
        //удаляем пробелы с конца и начала
        if(phone != null){
            this.phone = phone.trim();
        }

        this.mail = mail;


        //если не задан протокол
        if(site != null){
            if(!site.startsWith("http://") && !site.startsWith("https://")){
                this.site = "http://" + site;
            } else {
                this.site = site;
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{
                this.name,
                this.avatar,
                this.about,
                this.tags,
                this.status,
                this.price,
                this.location,
                this.age,
                this.experience,
                this.ownership,
                this.mentalTags,
                this.specialty,
                this.phone,
                this.mail,
                this.site
        });
    }

    public static final Creator<Freelancer> CREATOR = new Creator<Freelancer>() {
        @Override
        public Freelancer createFromParcel(Parcel source) {
            return new Freelancer(source);
        }

        @Override
        public Freelancer[] newArray(int size) {
            return new Freelancer[size];
        }
    };
}
