package com.zv.freelansimclient.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created with IntelliJ IDEA.
 * User: dimon
 * Date: 07.06.13
 * Time: 23:32
 * To change this template use File | Settings | File Templates.
 */
public class ShortTask implements Parcelable{
    private String title;
    private String price;
    private String published;
    private String tags;
    private int id;

    public ShortTask(String title, String price, String published, String tags, int id) {
        this.title = title;
        this.price = price;
        this.published = published;
        this.tags = tags;
        this.id = id;
    }

    public ShortTask(Parcel in) {
        //title, price, published, tags
        String[] data = new String[4];

        this.id = in.readInt();
        in.readStringArray(data);
        this.title = data[0];
        this.price = data[1];
        this.published = data[2];
        this.tags = data[3];
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getPublished() {
        return published;
    }

    public String getTags() {
        return tags;
    }

    public int getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeStringArray(new String[]{
                this.title,
                this.price,
                this.published,
                this.tags
        });
    }

    public static final Creator<ShortTask> CREATOR = new Creator<ShortTask>() {
        @Override
        public ShortTask createFromParcel(Parcel in) {
                return new ShortTask(in);
        }

        @Override
        public ShortTask[] newArray(int size) {
            return new ShortTask[size];
        }
    };
}
