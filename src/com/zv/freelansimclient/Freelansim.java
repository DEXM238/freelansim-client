package com.zv.freelansimclient;

import com.zv.freelansimclient.models.Freelancer;
import com.zv.freelansimclient.models.ShortFreelancer;
import com.zv.freelansimclient.models.ShortTask;
import com.zv.freelansimclient.models.Task;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Zlyuka
 * Date: 24.07.13
 * Time: 23:27
 * To change this template use File | Settings | File Templates.
 *
 * На случай если для сайта сделают api
 */
public interface Freelansim {

    /**
     *
     * @return список, в котором содержится краткая информация о заказах
     */
    public List<ShortTask> getShortTasksList(int page);

    /**
     *
     * @return список, в котором содержится краткая информация о фрилансерах
     */
    public List<ShortFreelancer> getShortFreelancersList(int page, String query);

    /**
     *
     * @param taskId идентификатор задачи
     * @return подробную информацию о задаче
     */
    public Task getTask(int taskId);

    /**
     *
     * @param freelancerName имя пользователя на freelansim.ru
     * @return подробную информацию о фрилансере
     */
    public Freelancer getFreelancer(String freelancerName);
}
