package com.zv.freelansimclient;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: dimon
 * Date: 28.06.13
 * Time: 16:56
 * To change this template use File | Settings | File Templates.
 */
public class Utils {
    public static final int MAX_TAGS_SIZE = 50;

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static boolean[] readBooleanArrayPref(Context context, String key){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean[] result;

        try {
            JSONArray jsonArray = new JSONArray(prefs.getString(key, ""));
            if(jsonArray != null){
                int length = jsonArray.length();
                result = new boolean[length];

                for(int i=0; i < length; i++){
                    result[i] = jsonArray.getBoolean(i);
                }

                return result;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public static boolean writeBooleanArraPref(Context context, String key,  boolean[] values){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();

        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(Arrays.toString(values));
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

        editor.putString(key, jsonArray.toString());
        editor.commit();
        return true;
    }
}
