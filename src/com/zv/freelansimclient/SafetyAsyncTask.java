package com.zv.freelansimclient;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Zlyuka
 * Date: 04.09.13
 * Time: 22:29
 * To change this template use File | Settings | File Templates.
 */
abstract public class SafetyAsyncTask<E> extends AsyncTask<String, Void, List<E>> {
    private volatile boolean mRunning = true;
    private Context mContext;

    public SafetyAsyncTask(Context context) {
        this.mContext = context;
    }

    public boolean isRunning() {
        return mRunning;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        mRunning = false;
    }
}
