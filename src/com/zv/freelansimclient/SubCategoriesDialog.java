package com.zv.freelansimclient;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created with IntelliJ IDEA.
 * User: Zlyuka
 * Date: 15.08.13
 * Time: 0:08
 * To change this template use File | Settings | File Templates.
 */
public class SubCategoriesDialog extends DialogFragment
        implements DialogInterface.OnMultiChoiceClickListener, DialogInterface.OnClickListener{

    //массив заголовков для выбора
    private int mTitlesArrayId;

    //отмеченные элементы
    private boolean[] mCheckedItems;

    private Resources mResources;

    public SubCategoriesDialog(int titlesArrayId) {
        this.mTitlesArrayId = titlesArrayId;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mResources = getResources();

        mCheckedItems = Utils.readBooleanArrayPref(getActivity(), mResources.getResourceName(mTitlesArrayId));
        if(mCheckedItems == null){
            mCheckedItems = new boolean[mResources.getStringArray(mTitlesArrayId).length];
        }

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.sub_categories_dialog_title)
                .setMultiChoiceItems(mTitlesArrayId, mCheckedItems, this)
                .setPositiveButton(android.R.string.ok, this);
        return adb.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        mCheckedItems[which] = isChecked;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch(which){
            case AlertDialog.BUTTON_POSITIVE:
                Utils.writeBooleanArraPref(getActivity(), getResources().getResourceName(mTitlesArrayId), mCheckedItems);
                break;
        }
    }
}
