package com.zv.freelansimclient.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.zv.freelansimclient.R;
import com.zv.freelansimclient.SubCategoriesDialog;
import com.zv.freelansimclient.ThreeStateCheckBox;
import com.zv.freelansimclient.Utils;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Zlyuka
 * Date: 11.08.13
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
*/
public class CategoriesAdapter extends ArrayAdapter<String>
        implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    public static final String CATEGORIES_PREFS = "checed_categories";

    private final Context mContext;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mEditor;
    private Resources mResources;
    private FragmentManager mFragmentManager;
    private DialogFragment mSubCategoriesDialog;
    private LayoutInflater mInflater;

    private final String[] mItems;
    private final String[] mItemsValues;
    //отмеченые категории
    private boolean[] mItemsChecked;

    public CategoriesAdapter(Context context, String[] items, String[] itemsValues){
        super(context, R.layout.categories_list_item, R.id.titleTextView, items);
        this.mContext = context;
        this.mItems = items;
        this.mItemsValues = itemsValues;

        this.mItemsChecked = Utils.readBooleanArrayPref(mContext, CATEGORIES_PREFS);

        if(this.mItemsChecked == null){
            this.mItemsChecked = new boolean[mItemsValues.length];
        }

        this.mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        this.mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //текущая строка
        mItemsChecked[getViewHolder((View)buttonView.getParent().getParent()).position] = isChecked;

        mEditor = mPrefs.edit();
        mEditor.putString(CATEGORIES_PREFS, Arrays.toString(mItemsChecked));
        mEditor.commit();
    }

    @Override
    public void onClick(View v) {
        if(mResources == null){
            mResources = mContext.getResources();
        }

        if(mFragmentManager == null) {
            FragmentActivity fm = (FragmentActivity)mContext;
            mFragmentManager = fm.getSupportFragmentManager();
        }

        //идентификатор массива в котором хранятся подкатегории: web, mobile, app и т.д.
        int resId = mResources.getIdentifier(mItemsValues[getViewHolder(v).position], "array", mContext.getPackageName());

        mSubCategoriesDialog = new SubCategoriesDialog(resId);

        mSubCategoriesDialog.show(mFragmentManager, DialogFragment.class.getName());
    }

    private static class ViewHolder{
        public TextView titleTextView;
        public ThreeStateCheckBox categoryCheckBox;
        public View quickContext;
        public int position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if(rowView == null) {
            rowView = mInflater.inflate(R.layout.categories_list_item, parent , false);

            holder = new ViewHolder();
            holder.titleTextView = (TextView) rowView.findViewById(R.id.titleTextView);
            holder.categoryCheckBox = (ThreeStateCheckBox)rowView.findViewById(R.id.categoryCheckBox);
            holder.quickContext = rowView.findViewById(R.id.quickContext);

            holder.quickContext.setOnClickListener(this);
            holder.categoryCheckBox.setIndeterminate(true);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder)rowView.getTag();
        }

        holder.titleTextView.setText(mItems[position]);

        holder.categoryCheckBox.setOnCheckedChangeListener(null);
        if(mItemsChecked[position]){
            holder.categoryCheckBox.setChecked(true);
        } else {
            holder.categoryCheckBox.setChecked(false);
        }
        holder.categoryCheckBox.setOnCheckedChangeListener(this);

        holder.position = position;

        return rowView;
    }

    @Override
    public int getCount() {
        if(mItems != null){
            return mItems.length;
        }
        return 0;
    }

    @Override
    public String getItem(int position) {
        return mItems[position];
    }

    /**
     *
     * @param v View, для которого нужно получить ViewHolder
     * @return  ViewHolder, в котором содержится переданный View
     */
    private ViewHolder getViewHolder(View v){
        try {
            return  (ViewHolder)((View)v.getParent()).getTag();
        } catch(ClassCastException e){
            e.printStackTrace();
            return null;
        }
    }
}
