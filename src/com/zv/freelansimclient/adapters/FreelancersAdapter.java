package com.zv.freelansimclient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.zv.freelansimclient.DrawableManager;
import com.zv.freelansimclient.R;
import com.zv.freelansimclient.Utils;
import com.zv.freelansimclient.models.ShortFreelancer;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dimon
 * Date: 30.06.13
 * Time: 15:39
 * To change this template use File | Settings | File Templates.
 */
public class FreelancersAdapter extends ArrayAdapter<ShortFreelancer> {
    private final Context context;
    private final List<ShortFreelancer> values;

    private DrawableManager drawableManager;

    public FreelancersAdapter(Context context, List<ShortFreelancer> values) {
        super(context, R.layout.freelancers_row);
        this.context = context;
        this.values = values;

        this.drawableManager = new DrawableManager();
    }

    static class ViewHolder {
        public TextView nameTextView;
        public TextView tagsTextView;
        public TextView priceTextView;
        public TextView descriptionTextView;
        public ImageView avatarImageView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if(rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            rowView = inflater.inflate(R.layout.freelancers_row, parent , false);
            holder = new ViewHolder();
            holder.nameTextView = (TextView) rowView.findViewById(R.id.nameTextView);
            holder.tagsTextView = (TextView) rowView.findViewById(R.id.tagsTextView);
            holder.priceTextView = (TextView) rowView.findViewById(R.id.priceTextView);
            holder.descriptionTextView = (TextView) rowView.findViewById(R.id.descriptionTextView);
            holder.avatarImageView = (ImageView) rowView.findViewById(R.id.avatarImageView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder)rowView.getTag();
        }
        ShortFreelancer item = getItem(position);

        //урезаем теги
        String tags = item.getTags();
        if(tags.length() > Utils.MAX_TAGS_SIZE) {
            tags = tags.substring(0, Utils.MAX_TAGS_SIZE);
            tags = tags.substring(0, tags.lastIndexOf(','));
            tags += "...";
        }
        holder.nameTextView.setText(item.getName());
        holder.tagsTextView.setText(tags);
        holder.priceTextView.setText(item.getPrice());
        holder.descriptionTextView.setText(item.getDescription());

        String avatar = item.getAvatar();
        if(avatar == null) {
            holder.avatarImageView.setImageResource(R.drawable.social_person);
        } else {
            drawableManager.fetchDrawableOnThread("http://freelansim.ru" + avatar, holder.avatarImageView);
        }

        return rowView;
    }

    @Override
    public int getCount() {
        if(values != null) {
            return values.size();
        }
        return 0;
    }

    @Override
    public ShortFreelancer getItem(int position) {
        return values.get(position);
    }
}
