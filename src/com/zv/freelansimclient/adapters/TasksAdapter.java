package com.zv.freelansimclient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zv.freelansimclient.R;
import com.zv.freelansimclient.models.ShortTask;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dimon
 * Date: 08.06.13
 * Time: 13:08
 * To change this template use File | Settings | File Templates.
 */
public class TasksAdapter extends ArrayAdapter<ShortTask> {
    private final Context context;
    private final List<ShortTask> values;

    public TasksAdapter(Context context, List<ShortTask> values) {
        super(context, R.layout.tasks_row);
        this.context = context;
        this.values = values;
    }

    static class ViewHolder {
        public TextView titleTextView;
        public TextView tagsTextView;
        public TextView priceTextView;
        public TextView publishedTextView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if(rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            rowView = inflater.inflate(R.layout.tasks_row, parent , false);
            holder = new ViewHolder();
            holder.titleTextView = (TextView) rowView.findViewById(R.id.titleTextView);
            holder.tagsTextView = (TextView)rowView.findViewById(R.id.tagsTextView);
            holder.priceTextView = (TextView)rowView.findViewById(R.id.priceTextView);
            holder.publishedTextView = (TextView)rowView.findViewById(R.id.publishedTextView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder)rowView.getTag();
        }
        ShortTask item = getItem(position);
        holder.titleTextView.setText(item.getTitle());
        holder.tagsTextView.setText(item.getTags());
        holder.priceTextView.setText(item.getPrice());
        holder.publishedTextView.setText(item.getPublished());
        return rowView;
    }

    @Override
    public int getCount() {
        if(values != null) {
            return values.size();
        }
        return 0;
    }

    @Override
    public ShortTask getItem(int position) {
        return values.get(position);
    }
}
