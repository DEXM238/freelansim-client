package com.zv.freelansimclient.fragments;

import android.widget.ArrayAdapter;
import com.zv.freelansimclient.FreelansimJsoupParser;
import com.zv.freelansimclient.activities.TaskActivity;
import com.zv.freelansimclient.adapters.TasksAdapter;
import com.zv.freelansimclient.models.ShortTask;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Zlyuka
 * Date: 30.06.13
 * Time: 23:56
 * To change this template use File | Settings | File Templates.
 */
public class TasksListFragment extends AsyncListFragment<ShortTask>{
    public TasksListFragment() {
        setOnClick(TaskActivity.class, TaskActivity.EXT_ITEM_TASK);
    }

    @Override
    protected List<ShortTask> getItems(String... params) {
        return new FreelansimJsoupParser(getActivity()).getShortTasksList(mCurrentPage);
    }

    @Override
    protected ArrayAdapter<ShortTask> setAdapter() {
        return new TasksAdapter(getActivity(), this.items);
    }
}