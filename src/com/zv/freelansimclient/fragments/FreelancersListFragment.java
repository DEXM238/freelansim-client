package com.zv.freelansimclient.fragments;

import android.widget.ArrayAdapter;
import com.zv.freelansimclient.FreelansimJsoupParser;
import com.zv.freelansimclient.activities.FreelancerActivity;
import com.zv.freelansimclient.adapters.FreelancersAdapter;
import com.zv.freelansimclient.models.ShortFreelancer;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dimon
 * Date: 30.06.13
 * Time: 0:05
 * Список фрилансеров с краткой информацией о каждом
 */
public class FreelancersListFragment extends AsyncListFragment<ShortFreelancer>{
    public FreelancersListFragment() {
        setOnClick(FreelancerActivity.class, FreelancerActivity.EXT_ITEM_FREELANCER);
    }

    @Override
    protected List<ShortFreelancer> getItems(String... params) {
        return new FreelansimJsoupParser(getActivity()).getShortFreelancersList(mCurrentPage, params[0]);
    }

    @Override
    protected ArrayAdapter<ShortFreelancer> setAdapter() {
        return new FreelancersAdapter(getActivity(), this.items);
    }
}