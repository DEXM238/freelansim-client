package com.zv.freelansimclient.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import com.zv.freelansimclient.*;
import com.zv.freelansimclient.activities.MainActivity;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshAttacher;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dimon
 * Date: 22.09.13
 * Time: 17:45
 * To change this template use File | Settings | File Templates.
 */
public abstract class AsyncListFragment<T extends Parcelable> extends ListFragment implements PullToRefreshAttacher.OnRefreshListener {
    protected List<T> items = new ArrayList<T>();
    protected ListView mListView;
    protected SafetyAsyncTask<T> mTask;
    protected PullToRefreshAttacher mPullToRefreshAttacher;

    protected ArrayAdapter<T> adapter;

    protected int mCurrentPage = 1;

    //Для плавного скролла
    protected int mLastVisiblePosition;
    protected int mFirstVisiblePosition;
    protected SmoothScrollRunnable mSmoothScrollRunnable = new SmoothScrollRunnable();

    //для обработчика клика
    private Class activityClass;
    private String extraName;

    public void setOnClick(Class activityClass, String extraName){
        this.activityClass = activityClass;
        this.extraName = extraName;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        mPullToRefreshAttacher = ((MainActivity)getActivity()).getPullToRefreshAttacher();
        mListView = getListView();
        mListView.setOnScrollListener(new ItemsScroll());

        View footerView = this.getActivity().getLayoutInflater()
                .inflate(R.layout.footer_view, null, false);
        mListView.addFooterView(footerView);

        mPullToRefreshAttacher.setRefreshableView(mListView, this);

        if(Utils.isOnline(getActivity())) {
            startTask("");
        } else {
            Toast.makeText(getActivity(), R.string.check_internet_status, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent detailActivity = new Intent(getActivity(), activityClass);
        T item = items.get(position);

        detailActivity.putExtra(extraName, item);
        startActivity(detailActivity);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        SearchView searchView = (SearchView)menu.findItem(R.id.menu_search).getActionView();
        Log.wtf("ololo", "options menu");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mCurrentPage = 1;
                items.clear();
                startTask(query);
                Log.v("ololo", "test submit");
                Log.wtf("ololo", "test");
                Log.wtf("ololo", query);

                return true;  //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.v("ololo", "test change");
                return false;  //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onRefreshStarted(View view) {
        mCurrentPage = 1;
        items.clear();
        startTask("");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mTask != null) {
            mTask.cancel(true);
        }
    }

    protected void startTask(String query) {
        if(mTask == null) {
            mTask = new GetItems(getActivity());
        } else if(mTask.getStatus() == AsyncTask.Status.RUNNING) {
            mTask.cancel(true);
        }
        try {
            mTask.execute(query);
        } catch(IllegalStateException e) {
            mTask.cancel(true);
            mTask = new GetItems(getActivity());
            mTask.execute(query);
        }
    }

    protected abstract List<T> getItems(String... params);
    protected abstract ArrayAdapter<T> setAdapter();

    private class ItemsScroll extends EndlessScrollListener {

        @Override
        protected void addItems() {
            startTask("");
        }
    }

    protected class SmoothScrollRunnable implements Runnable {

        @Override
        public void run() {
            mListView.smoothScrollToPosition(mLastVisiblePosition);
        }
    }

    protected class GetItems extends SafetyAsyncTask<T> {

        public GetItems(Context context) {
            super(context);
        }

        @Override
        protected List<T> doInBackground(String... params) {
            if(isRunning()){
                return getItems(params);
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<T> shortTasks) {
            super.onPostExecute(shortTasks);
            Log.v("ololo", "onPostExecute " + mCurrentPage);
            mFirstVisiblePosition = mListView.getFirstVisiblePosition();
            mLastVisiblePosition = mListView.getLastVisiblePosition();
            AsyncListFragment.this.items.addAll(shortTasks);
            adapter = setAdapter();
            setListAdapter(adapter);
            adapter.notifyDataSetChanged();
            mCurrentPage++;
            mPullToRefreshAttacher.setRefreshComplete();
            mListView.setSelectionFromTop(mFirstVisiblePosition, 0);
            mListView.post(mSmoothScrollRunnable);
        }
    }
}
