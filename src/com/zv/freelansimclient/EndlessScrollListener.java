package com.zv.freelansimclient;

import android.util.Log;
import android.widget.AbsListView;

/**
 * Created with IntelliJ IDEA.
 * User: Zlyuka
 * Date: 26.07.13
 * Time: 0:06
 * To change this template use File | Settings | File Templates.
 */
public abstract class EndlessScrollListener implements AbsListView.OnScrollListener {
    private boolean loading = true;
    private int currentPage = 0;
    private int previousTotal = 0;
    private int visibleThreshold = 5;

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        if((firstVisibleItem + visibleItemCount) == totalItemCount && (totalItemCount != 0)) {
            Log.v("ololo", "total count: "+totalItemCount);
            addItems();
        }
    }

    abstract protected void addItems();
}
