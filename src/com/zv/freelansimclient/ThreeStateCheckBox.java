package com.zv.freelansimclient;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created with IntelliJ IDEA.
 * User: dimon
 * Date: 27.10.13
 * Time: 12:48
 * Чекбокс с неопределенным состоянием
 */
public class ThreeStateCheckBox extends FrameLayout {
    private ImageView mIndeterminateImage;
    private CheckBox mCheckBox;

    public ThreeStateCheckBox(Context context) {
        super(context);
        initUI();
    }

    public ThreeStateCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI();
    }

    public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener listener){
        mCheckBox.setOnCheckedChangeListener(listener);
    }

    public void setChecked(boolean checked){
        mCheckBox.setChecked(checked);
    }

    public void setIndeterminate(boolean flag){
        if(flag){
            mCheckBox.setVisibility(INVISIBLE);
            mIndeterminateImage.setVisibility(VISIBLE);
        } else {
            mCheckBox.setVisibility(VISIBLE);
            mIndeterminateImage.setVisibility(INVISIBLE);
        }
    }

    private void initUI(){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.three_state_check_box, this);
        mIndeterminateImage = (ImageView)findViewById(R.id.indeterminateImage);
        mCheckBox = (CheckBox)findViewById(R.id.checkBox);

        mIndeterminateImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ThreeStateCheckBox.this.setIndeterminate(false);
            }
        });

    }
}
